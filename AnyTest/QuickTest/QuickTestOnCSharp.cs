﻿using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using NUnit.Framework;
using System.Text;
using System;
using System.Dynamic;

namespace QuickTest
{
    [TestFixture]
    public class QuickTestOnCSharp
    {
        [Test]
        public void TestLang()
        {
            var hop = CultureInfo.GetCultures(CultureTypes.AllCultures);
            var t = new List<CultureInfo>();

            t.Add(CultureInfo.GetCultureInfo("fr-fr"));
            t.Add(CultureInfo.GetCultureInfo("en-gb"));
            t.Add(CultureInfo.GetCultureInfo("es-es"));
            t.Add(CultureInfo.GetCultureInfo("he-il"));
            t.Add(CultureInfo.GetCultureInfo("it-it"));
            t.Add(CultureInfo.GetCultureInfo("de-de"));
            t.Add(CultureInfo.GetCultureInfo("pt-br"));
            t.Add(CultureInfo.GetCultureInfo("zh-cn"));
            t.Add(CultureInfo.GetCultureInfo("tr-tr"));
            t.Add(CultureInfo.GetCultureInfo("nl-nl"));
            t.Add(CultureInfo.GetCultureInfo("cs-cz"));
            t.Add(CultureInfo.GetCultureInfo("sk-sk"));
            t.Add(CultureInfo.GetCultureInfo("bg-bg"));
            t.Add(CultureInfo.GetCultureInfo("hu-hu"));
            t.Add(CultureInfo.GetCultureInfo("ro-ro"));
            t.Add(CultureInfo.GetCultureInfo("pl-pl"));
            t.Add(CultureInfo.GetCultureInfo("lt-lt"));
            t.Add(CultureInfo.GetCultureInfo("ko-kr"));
            t.Add(CultureInfo.GetCultureInfo("ja-jp"));
            t.Add(CultureInfo.GetCultureInfo("sv-se"));
            t.Add(CultureInfo.GetCultureInfo("da-dk"));
            t.Add(CultureInfo.GetCultureInfo("ru-ru"));

            var english = new StringBuilder();
            var french = new StringBuilder();
            foreach (var lang in hop.Where(p => t.Contains(p)))
            {
                english.Append("<data name=\"" + lang.Name + "\" xml:space=\"preserve\">"
    + "< value >" + lang.EnglishName + "</ value >"
    + "</ data >");

                french.Append("<data name=\"" + lang.Name + "\" xml:space=\"preserve\">"
    + "< value >" + lang.DisplayName + "</ value >"
    + "</ data >");
            }

            Console.WriteLine(english.ToString());
            Console.WriteLine(french.ToString());            
        }



        [Test]
        public void TestHashSet()
        {
            var a = new HashSet<int>();

            a.Add(5);
            a.Add(1);
            a.Add(1);
            a.Add(1);

            Assert.That(a.Count, Is.EqualTo(2));

            Assert.That(a.First(), Is.EqualTo(5));
            Assert.That(a.Last(), Is.EqualTo(1));
        }

        [Test]
        public void TestValidatorDontCallValidateOnError()
        {
            var toValidate = new ValidatedClass();

            var vc = new ValidationContext(toValidate);
            vc.DisplayName = "toto";
            var res = new List<ValidationResult>();
            var result = Validator.TryValidateObject(toValidate, vc, res, true);

            Assert.That(result, Is.False);
            Assert.That(res.Count, Is.EqualTo(2));
            Console.WriteLine(string.Join(";", res.Select(x => x.ErrorMessage)));
            Console.WriteLine(string.Join(",", res.Select(x => string.Join(",", x.MemberNames))));
        }

        [Test]
        public void TestValidator()
        {
            var toValidate = new ValidatedClass();
            toValidate.s = "s";
            toValidate.s1 = "s1";

            var vc = new ValidationContext(toValidate);
            vc.DisplayName = "toto";
            var res = new List<ValidationResult>();
            var result = Validator.TryValidateObject(toValidate, vc, res, true);

            Assert.That(result, Is.False);
            Assert.That(res.Count, Is.EqualTo(4));
            Console.WriteLine(string.Join(";", res.Select(x => x.ErrorMessage)));
            Console.WriteLine(string.Join(",", res.Select(x => string.Join(",", x.MemberNames))));
        }

        [Test]
        public void TestValidate()
        {
            var toValidate = new ValidatedClass();

            var vc = new ValidationContext(this);
            var res = toValidate.Validate(vc);
            
            Assert.That(res.Count, Is.EqualTo(4));
            Console.WriteLine(string.Join(";", res.Select(x => x.ErrorMessage)));
            Console.WriteLine(string.Join(",", res.Select(x => string.Join(",", x.MemberNames))));
        }

        [Test]
        public void TestValidatorProperty()
        {
            var toValidate = new ValidatedClass();

            var vc = new ValidationContext(toValidate);
            vc.MemberName = "s";
            var res = new List<ValidationResult>();
            var result = Validator.TryValidateProperty(null, vc, res);

            Assert.That(res.Count, Is.EqualTo(1));
        }

        [Test]
        public void TestSortedSet()
        {
            var a = new SortedSet<int>();

            a.Add(5);
            a.Add(1);
            a.Add(1);

            Assert.That(a.Count, Is.EqualTo(2));

            Assert.That(a.First(), Is.EqualTo(1));
            Assert.That(a.Last(), Is.EqualTo(5));

        }

        [Test]
        public void TestNameOf()
        {
            var toValidate = new ValidatedClass();
            
            Assert.That(nameof(toValidate.s1), Is.EqualTo("s1"));
            Assert.That(toValidate.nameS1, Is.EqualTo("s1"));
        }

        [Test]
        public void TestExpendoObject()
        {
            dynamic hop = new ExpandoObject();

            Assert.That(hop.Toto, Is.EqualTo(2));
        }

        private void Plop(IToto hop)
        {
            hop.toto = 2;
        }
    }
}
