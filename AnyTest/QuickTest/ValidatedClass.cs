﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace QuickTest
{
    public class ValidatedClass : IValidatableObject
    {
        [Required]
        [Display( Name = "hop")]
        public string s { get; set; }

        public bool True = true;
        public bool False = true;

        [Required]
        [Display(Name = "hip")]
        public string s1 { get; set; }

        public string nameS1 = nameof(s1);

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            yield return new ValidationResult($"zoubon {validationContext.DisplayName}");
            yield return new ValidationResult($"zoubon1 {validationContext.DisplayName}");
            yield return new ValidationResult($"zoubon2 {validationContext.DisplayName}");
            yield return new ValidationResult($"zoubon3 {validationContext.DisplayName}");
        }
    }
}
