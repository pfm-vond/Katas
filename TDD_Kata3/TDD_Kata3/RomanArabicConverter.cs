﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDD_Kata3
{
    public class RomanArabicConverter
    {
        public static void Main()
        {
            
        }
        
        private Dictionary<char, char> tenMult;
        private Dictionary<char, int> romanUnitDic;

        public RomanArabicConverter()
        {
            this.tenMult = new Dictionary<char, char>();
            this.tenMult.Add('I', 'X');
            this.tenMult.Add('V', 'L');
            this.tenMult.Add('X', 'C');
            this.tenMult.Add('L', 'D');
            this.tenMult.Add('C', 'M');

            this.romanUnitDic = new Dictionary<char, int>();

            this.romanUnitDic.Add('I', 1);
            this.romanUnitDic.Add('V', 5);
            this.romanUnitDic.Add('X', 10);
            this.romanUnitDic.Add('L', 50);
            this.romanUnitDic.Add('C', 100);
            this.romanUnitDic.Add('D', 500);
            this.romanUnitDic.Add('M', 1000);

        }
        
        public string ConvertArabicUnitToRoman(int n)
        {
            var hop = new StringBuilder();
            if (n % 5 == 4)
            {
                hop.Append("I");
                n = n + 1;
            }

            if (n >= 10)
            {
                hop.Append("X");
                n = n - 10;
            }

            if (n >= 5)
            {
                hop.Append("V");
                n = n - 5;
            }

            while (n > 0)
            {
                n--;
                hop.Append("I");
            }

            return hop.ToString();
        }

        public string MultiplyByTen(string entry)
        {
            var s = new StringBuilder();
            foreach (var c in entry)
            {
                s.Append(this.tenMult[c]);
            }

            return s.ToString();
        }
        
        public string ConvertToRoman(int arabicNumber)
        {
            string result = string.Empty;
            foreach (var c in arabicNumber.ToString())
            {
                result = this.MultiplyByTen(result);
                result += this.ConvertArabicUnitToRoman(int.Parse(c.ToString()));
            }

            return result;
        }

        public int ConvertToArabic(string romanNumber)
        {
            int result = 0;
            int lastValueAdded = 0;
            for (int i = romanNumber.Length - 1; i >= 0; i--)
            {
                if (lastValueAdded > this.romanUnitDic[romanNumber[i]])
                {
                    result -= this.romanUnitDic[romanNumber[i]];
                }
                else
                {
                    result += this.romanUnitDic[romanNumber[i]];
                    lastValueAdded = this.romanUnitDic[romanNumber[i]];
                }
            }
            return result;
        }

    }
}
