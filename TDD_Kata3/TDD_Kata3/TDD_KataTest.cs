﻿// <copyright file="TDD_KataTest.cs" company="Talensoft">
//     Talensoft All rights reserved.
// </copyright>
// <author>VONDERSCHER Pierre</author>
namespace TDD_Kata3
{
    using NUnit.Framework;

    /// <summary>
    /// Test class associated to KataFizzBuzz class
    /// </summary>
    [TestFixture]
    internal class TDD_KataTest
    {
        private RomanArabicConverter Rac { get; set; }
        
        [SetUp]
        public void PreTestInit()
        {
            this.Rac = new RomanArabicConverter();
        }
        
        [TestCase("I", "X")]
        [TestCase("LII", "DXX")]
        [TestCase("XXXVII", "CCCLXX")]
        [TestCase("LXIX", "DCXC")]
        public void TestMultiplyByTen(string roman, string result)
        {
            Assert.AreEqual(this.Rac.MultiplyByTen(roman), result);
        }

        [TestCase("I", 1)]
        [TestCase("LII", 52)]
        [TestCase("XXXVII", 37)]
        [TestCase("LXIX", 69)]
        [TestCase("MCMIII", 1903)]
        public void TestConversionToRoman(string roman, int n)
        {
            Assert.AreEqual(this.Rac.ConvertToRoman(n), roman);
        }

        [TestCase("I", 1)]
        [TestCase("LII", 52)]
        [TestCase("XXXVII", 37)]
        [TestCase("LXIX", 69)]
        [TestCase("MCMIII", 1903)]
        public void TestConversionToArabic(string roman, int n)
        {
            Assert.AreEqual(this.Rac.ConvertToArabic(roman), n);
        }
    }
}
