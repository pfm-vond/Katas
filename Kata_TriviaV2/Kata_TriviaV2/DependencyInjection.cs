﻿using Autofac;
using Kata_TriviaV2.ConsoleOutput;
using Kata_TriviaV2.Public;
using System;
using Trivia_csharp;

namespace Kata_TriviaV2
{
    public static class DependencyInjection
    {
        private readonly static Lazy<IContainer> _builder = new Lazy<IContainer>(() => InitializeDI());

        public static IContainer Builder => _builder.Value;

        private static IContainer InitializeDI()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<Trivia>().InstancePerRequest();

            RegisterTriviaBinding(builder);

            builder.RegisterModule(new OutputModule());
            
            return builder.Build();
        }

        private static void RegisterTriviaBinding(ContainerBuilder builder)
        {
            RegisterPlayerBinding(builder);
            RegisterQuestionBinding(builder);
        }

        private static void RegisterQuestionBinding(ContainerBuilder builder)
        {
            builder.RegisterType<Question>().As<IQuestion>();
            builder.RegisterType<QuestionPool>().As<IQuestionPool>();
        }

        private static void RegisterPlayerBinding(ContainerBuilder builder)
        {
            builder.RegisterType<Player>().As<IPlayer>();
            builder.RegisterType<PlayerPool>().As<IPlayerPool>();
        }
    }
}
