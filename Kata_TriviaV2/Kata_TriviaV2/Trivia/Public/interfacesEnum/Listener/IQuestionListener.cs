﻿namespace Kata_TriviaV2.Public
{
    public interface IQuestionListener
    {
        void CorrectAnswer();
        void WrongAnswer();
    }
}
