﻿namespace Kata_TriviaV2.Public
{
    public interface ITextWriterProvider
    {
        System.IO.TextWriter Get();
    }
}