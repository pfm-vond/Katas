﻿using Autofac;
using Kata_TriviaV2.Public;

namespace Kata_TriviaV2.ConsoleOutput
{
    public class OutputModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<TriviaConsoleOutput>().As<ITriviaListener>().SingleInstance();
            builder.RegisterType<PlayerConsoleOutput>().As<IPlayerListener>().SingleInstance();
            builder.RegisterType<QuestionConsoleOutput>().As<IQuestionListener>().SingleInstance();
            builder.RegisterType<ConsoleOutputProvider>().As<ITextWriterProvider>().SingleInstance();
        }
    }
}
