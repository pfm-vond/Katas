﻿namespace Kata_it2.FizzBuzz
{
    using Handler;

    public class FizzBuzzLoop
    {
        private int CurrentPos { get; set; }
        
        public void Reset()
        {
            this.CurrentPos = 0;
        }

        public string Next()
        {
            return (++this.CurrentPos).GetFizzBuzzValue();
        }

        public bool HasNext()
        {
            return this.CurrentPos < 100;
        }
    }
}
