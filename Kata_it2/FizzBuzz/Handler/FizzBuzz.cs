﻿namespace Kata_it2.FizzBuzz.Handler
{
    using InterfacesAndAbstract;
    using ChainHandler;

    public class FizzBuzz
    {
        private AFizzBuzzChainHandler Fb { get; }

        private FizzBuzz()
        {
            var defaultHandler = new DefaultFizzBuzzHandler();

            var three = new FizzForThreeHandler();
            three.SetSuccessor(defaultHandler);

            var five = new BuzzForFiveHandler();
            five.SetSuccessor(three);

            this.Fb = new FizzBuzzForBothThreeAndFiveHandler();
            this.Fb.SetSuccessor(five);
        }

        public static FizzBuzz Instance { get; } = new FizzBuzz();

        public string GetFizzBuzzValue(int number)
        {
            return this.Fb.GetFizzBuzzValue(number);
        }
    }

    public static class ExtentionForFizzBuzz
    {
        public static string GetFizzBuzzValue(this int number)
        {
            var f = FizzBuzz.Instance;
            return f.GetFizzBuzzValue(number);
        }
    }
}
