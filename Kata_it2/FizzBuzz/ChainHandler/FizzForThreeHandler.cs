﻿namespace Kata_it2.FizzBuzz.ChainHandler
{
    using Kata_it2.InterfacesAndAbstract;

    public class FizzForThreeHandler : AFizzBuzzChainHandler
    {
        protected override string HandleValue(int number)
        {
            return "Fizz";
        }

        protected override bool CanHandle(int number)
        {
            return number % 3 == 0;
        }
    }
}
