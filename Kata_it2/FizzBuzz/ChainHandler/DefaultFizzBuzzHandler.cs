﻿namespace Kata_it2.FizzBuzz.ChainHandler
{
    using Kata_it2.InterfacesAndAbstract;

    public class DefaultFizzBuzzHandler : AFizzBuzzChainHandler
    {
        protected override string HandleValue(int number)
        {
            return number.ToString();
        }

        protected override bool CanHandle(int number)
        {
            return true;
        }
    }
}
