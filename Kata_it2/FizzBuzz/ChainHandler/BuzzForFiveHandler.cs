﻿namespace Kata_it2.FizzBuzz.ChainHandler
{
    using Kata_it2.InterfacesAndAbstract;

    public class BuzzForFiveHandler : AFizzBuzzChainHandler
    {
        protected override string HandleValue(int number)
        {
            return "Buzz";
        }

        protected override bool CanHandle(int number)
        {
            return number % 5 == 0;
        }
    }
}
