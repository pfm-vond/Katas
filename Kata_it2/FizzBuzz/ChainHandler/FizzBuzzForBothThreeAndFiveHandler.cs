﻿namespace Kata_it2.FizzBuzz.ChainHandler
{
    using Kata_it2.InterfacesAndAbstract;

    public class FizzBuzzForBothThreeAndFiveHandler : AFizzBuzzChainHandler
    {
        protected override string HandleValue(int number)
        {
            return "FizzBuzz";
        }

        protected override bool CanHandle(int number)
        {
            return number % 5 == 0 && number % 3 == 0;
        }
    }
}
