﻿namespace Kata_it2.InterfacesAndAbstract
{
    public interface IFizzBuzzChainHandler
    {
        void SetSuccessor(AFizzBuzzChainHandler successorToSet);

        string GetFizzBuzzValue(int number);
    }
}
