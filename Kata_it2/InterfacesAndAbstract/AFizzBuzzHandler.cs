﻿namespace Kata_it2.InterfacesAndAbstract
{
    public abstract class AFizzBuzzChainHandler : IFizzBuzzChainHandler
    {
        private AFizzBuzzChainHandler successor;

        protected abstract string HandleValue(int number);
        protected abstract bool CanHandle(int number);

        public void SetSuccessor(AFizzBuzzChainHandler successorToSet)
        {
            this.successor = successorToSet;
        }

        public string GetFizzBuzzValue(int number)
        {
            if (this.CanHandle(number))
            {
                return this.HandleValue(number);
            }
            else if (this.successor != null)
            {
                return this.successor.GetFizzBuzzValue(number);
            }

            return string.Empty;
        }
    }
}
