﻿namespace Kata_it2.Test
{
    using FizzBuzz;
    using FizzBuzz.Handler;

    using NUnit.Framework;

    public class TestFizzBuzz
    {
        private FizzBuzzLoop f;

        [SetUp]
        public void SetUp()
        {
            this.f = new FizzBuzzLoop();
            this.f.Reset();
        }

        [Test]
        public void TestBottomValueIsOne()
        {
            Assert.AreEqual(this.f.Next(), "1");
        }

        [Test]
        public void TestTopValueIsHundred()
        {
            int n = 0;
            while (this.f.HasNext())
            {
                n++;
                this.f.Next();

                if (!this.f.HasNext())
                {
                    Assert.AreEqual(100, n);
                }
            }
        }

        [TestCase(3)]
        [TestCase(33)]
        [TestCase(57)]
        [TestCase(63)]
        [TestCase(99)]
        public void MultiplesOfThreePrintFizzInstead(int test)
        {
            Assert.AreEqual(test.GetFizzBuzzValue(), "Fizz");
        }

        [TestCase(10)]
        [TestCase(20)]
        [TestCase(50)]
        [TestCase(55)]
        [TestCase(95)]
        public void MultiplesOfFivePrintBuzzInstead(int test)
        {
            Assert.AreEqual(test.GetFizzBuzzValue(), "Buzz");
        }

        [TestCase(30)]
        [TestCase(60)]
        [TestCase(90)]
        [TestCase(15)]
        [TestCase(45)]
        [TestCase(75)]
        public void MultiplesOfBothFiveAndThreePrintFizzBuzzInstead(int test)
        {
            Assert.AreEqual(test.GetFizzBuzzValue(), "FizzBuzz");
        }
    }
}
