﻿namespace Kata_Bowling.Bowling.IFrame
{
    using System.Collections.Generic;

    using Kata_Bowling.Bowling.Definition.Base;
    using Kata_Bowling.Bowling.Definition.Interface;

    class Frame : IFrame
    {
        public IRoll LastRolls { get; private set; }

        public bool IsFull()
        {
            return LastRolls.Count == 2;
        }

        public void AddRoll(int pinFalled)
        {
            this.LastRolls = RollFactory.GetRollFor(pinFalled, this.LastRolls);
        }

        public Frame(IRoll r, Frame f)
        {
            this.LastRolls = r;
        }
        
        public int Score()
        {
            return this.LastRolls.Score();
        }
    }
}
