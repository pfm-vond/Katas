﻿namespace Kata_Bowling.Bowling
{
    using Kata_Bowling.Bowling.Definition.Interface;

    class SimpleRoll : IRoll
    {
        private int PinFalled { get; }
        private IRoll PreviousRolls { get; }

        public SimpleRoll(int pins, IRoll r)
        {
            this.PinFalled = pins;
            this.PreviousRolls = r;
        }

        public int Score()
        {
            return this.PinFalled + this.PreviousRolls?.Score() ?? 0;
        }

        public int Count
        {
            get
            {
                return 1 + this.PreviousRolls?.Count ?? 0;
            }
        }
    }
}
