﻿namespace Kata_Bowling.Bowling
{
    using Kata_Bowling.Bowling.Definition.Base;
    using Kata_Bowling.Bowling.Definition.Interface;
    class Game : IGame
    {
        private Definition.Interface.IFrame LastFrame { get; set; } = null;
        
        public void Roll(int pinFalled)
        {
            if (LastFrame.IsFull())
            {
                this.LastFrame.AddRoll(pinFalled);
            }
            else
            {

                this.LastFrame = FrameFactory.GetFrameFor(RollFactory.GetRollFor(pinFalled), LastFrame);
            }
        }

        public int Score()
        {
            return this.LastFrame.Score();
        }
    }
}
