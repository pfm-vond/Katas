﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata_Bowling.Bowling.Definition.Interface
{
    public interface IFrame
    {
        bool IsFull();

        void AddRoll(int pinFalled);

        int Score();
    }
}
