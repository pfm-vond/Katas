﻿namespace Kata_Bowling.Bowling.Definition.Interface
{
    using System.Xml.Schema;

    public interface IRoll
    {
        int Score();

        int Count { get; }
    }
}
