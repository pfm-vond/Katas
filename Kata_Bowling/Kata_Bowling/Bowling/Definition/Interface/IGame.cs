﻿namespace Kata_Bowling.Bowling.Definition.Interface
{
    interface IGame
    {
        void Roll(int pinFalled);

        int Score();
    }
}
