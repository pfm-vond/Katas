﻿namespace Kata_Bowling.Bowling.Definition.Base
{
    using Kata_Bowling.Bowling.Definition.Interface;

    static class RollFactory
    {
        public static IRoll GetRollFor(int pins, IRoll r = null)
        {
            return new SimpleRoll(pins, r);
        }
    }
}
