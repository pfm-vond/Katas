﻿namespace Kata_Bowling.Bowling.Definition.Base
{
    using Kata_Bowling.Bowling.Definition.Interface;
    using Kata_Bowling.Bowling.IFrame;
    
    public static class FrameFactory
    {
        public static IFrame GetFrameFor(IRoll r, IFrame f)
        {
            return new Frame(r, null);
        }
    }
}
