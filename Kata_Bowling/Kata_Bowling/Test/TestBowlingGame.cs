﻿namespace Kata_Bowling.Test
{
    using System;

    using Kata_Bowling.Bowling;
    using Kata_Bowling.Bowling.Definition.Base;
    using Kata_Bowling.Bowling.Definition.Interface;

    using NUnit.Framework;

    [TestFixture]
    public class TestBowlingGame
    {
        [TestCase(0, 0)]
        [TestCase(1, 20)]
        public void Game(int pinFalled, int expectedScore)
        {
            IGame g = new Game();
            for (int i = 0; i <= 20; i++)
            {
                g.Roll(pinFalled);
            }

            Assert.AreEqual(expectedScore, g.Score());
        }

        [TestCase(1,5)]
        public void Spare(int pinFalledOnFirstRoll, int pinFalledOnThirdRoll)
        {
            IGame g = new Game();

            g.Roll(pinFalledOnFirstRoll);
            g.Roll(10 - pinFalledOnFirstRoll);

            Assert.AreEqual(10, g.Score());

            g.Roll(pinFalledOnThirdRoll);

            Assert.AreEqual(10 + 2 * pinFalledOnThirdRoll, g.Score());
        }
    }
}
