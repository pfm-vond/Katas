﻿namespace Kata_Bowling.Definition.Base
{
    using System;
    using Interface;

    public abstract class ChainHandler<TR, TP> : IChainHandler<TR, TP>
    {
        private IChainHandler<TR, TP> Successor { get; set; }

        private IHandler<TR, TP> Handler { get; }

        protected ChainHandler(IHandler<TR, TP> handler)
        {
            this.Handler = handler;
        }

        public void SetSuccessor(IChainHandler<TR, TP> successorToSet)
        {
            this.Successor = successorToSet;
        }

        public TR Execute(TP t)
        {
            if (this.Handler.CanHandle(t))
            {
                return this.Handler.Handle(t);
            }
            else if (this.Successor != null)
            {
                return this.Successor.Execute(t);
            }

            throw new NotSupportedException();
        }
    }
}
