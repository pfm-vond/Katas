﻿namespace Kata_Bowling.Definition.Interface
{
    public interface IChainHandler<TR, TP>
    {
        void SetSuccessor(IChainHandler<TR, TP> successorToSet);

        TR Execute(TP t);
    }
}
