﻿namespace Kata_Bowling.Definition.Interface
{
    public interface IHandler<out TR, in TP>
    {
        bool CanHandle(TP t);

        TR Handle(TP t);
    }
}
