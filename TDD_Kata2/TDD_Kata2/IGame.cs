﻿namespace Trivia_csharp.Init
{
    using System;

    public interface IGame
    {
        string CreateRockQuestion(int index);

        bool IsPlayable();

        int HowManyPlayers();

        bool Add(string playerName);

        void Roll(int roll);

        bool WasCorrectlyAnswered();

        bool WrongAnswer();
    }
}