﻿namespace TDD_Kata2
{
    using System;
    using System.IO;
    using System.Runtime.CompilerServices;

    using ApprovalTests;
    using ApprovalTests.Reporters;

    using NUnit.Framework;

    using Trivia_csharp.Init;

    /// <summary>
    /// Test class associated to KataFizzBuzz class
    /// </summary>
    [TestFixture]
    [UseReporter(typeof(DiffReporter))]
    internal class TestGame
    {
        public Game TestedGame { get; set; }

        /// <summary>
        /// pre-initialization method called before each test method of this class run.
        /// </summary>
        [SetUp]
        public void PreTestInit()
        {
            this.TestedGame = new Game();
        }

        [TestCase(3)]
        [TestCase(6)]
        [TestCase(51)]
        [TestCase(81)]
        public void CreateRockQuestion(int index)
        {
            Assert.IsTrue(this.TestedGame.CreateRockQuestion(index).EndsWith(index.ToString()));
            Assert.IsTrue(this.TestedGame.CreateRockQuestion(index).StartsWith("Rock Question "));
        }

        [TestCase(1, false)]
        [TestCase(2, true)]
        [TestCase(3, true)]
        [TestCase(4, true)]
        [TestCase(5, true)]
        public void IsPlayable(int nbPlayer, bool isPlayable)
        {
            for(int i = 0; i < nbPlayer; i++)
            {
                this.TestedGame.Add("player" + i);
            }

            Assert.AreEqual(this.TestedGame.IsPlayable(), isPlayable);
        }

        [TestCase(0)]
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(5)]
        public void Add(int nbPlayerToAdd)
        {
            for (int i = 0; i < nbPlayerToAdd; i++)
            {
                this.TestedGame.Add("player" + i);
            }

            Assert.AreEqual(nbPlayerToAdd, this.TestedGame.HowManyPlayers());
        }
        
        [TestCase()]
        public void HowManyPlayers()
        {
            var rand = new Random();
            int randInt = rand.Next(4);

            for (int i = 0; i < randInt; i++)
            {
                this.TestedGame.Add("player" + i);
            }

            var nbPlayerBeforeAdd = this.TestedGame.HowManyPlayers();
            this.TestedGame.Add(rand.Next(100000).ToString());

            Assert.AreEqual(nbPlayerBeforeAdd + 1, this.TestedGame.HowManyPlayers());
        }
        
        [TestCase(3, 1)]
        [TestCase(6, 1)]
        [TestCase(51, 1)]
        [TestCase(81, 1)]
        [TestCase(3, 2)]
        [TestCase(6, 2)]
        [TestCase(51, 2)]
        [TestCase(81, 2)]
        [TestCase(3, 5)]
        [TestCase(6, 5)]
        [TestCase(51, 5)]
        [TestCase(81, 5)]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public void Roll(int roll, int nbPlayer)
        {
            for (int i = 0; i < nbPlayer; i++)
            {
                this.TestedGame.Add("player" + i);
            }

            var consoleOut = new StringWriter();
            Console.SetOut(consoleOut);

            this.TestedGame.Roll(roll);

            Approvals.VerifyAll(consoleOut.ToString(), roll + " " + nbPlayer);
        }
        
        [TestCase(3, 1)]
        [TestCase(6, 1)]
        [TestCase(51, 1)]
        [TestCase(81, 1)]
        [TestCase(3, 2)]
        [TestCase(6, 2)]
        [TestCase(51, 2)]
        [TestCase(81, 2)]
        [TestCase(3, 5)]
        [TestCase(6, 5)]
        [TestCase(51, 5)]
        [TestCase(81, 5)]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public void WasCorrectlyAnswered(int roll, int nbPlayer)
        {
            for (int i = 0; i < nbPlayer; i++)
            {
                this.TestedGame.Add("player" + i);
            }

            this.TestedGame.Roll(roll);

            var consoleOut = new StringWriter();
            Console.SetOut(consoleOut);

            this.TestedGame.WasCorrectlyAnswered();

            Approvals.VerifyAll(consoleOut.ToString(), roll + " " + nbPlayer);
        }
        
        [TestCase(3, 1)]
        [TestCase(6, 1)]
        [TestCase(51, 1)]
        [TestCase(81, 1)]
        [TestCase(3, 2)]
        [TestCase(6, 2)]
        [TestCase(51, 2)]
        [TestCase(81, 2)]
        [TestCase(3, 5)]
        [TestCase(6, 5)]
        [TestCase(51, 5)]
        [TestCase(81, 5)]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public void WrongAnswer(int roll, int nbPlayer)
        {
            for (int i = 0; i < nbPlayer; i++)
            {
                this.TestedGame.Add("player" + i);
            }

            this.TestedGame.Roll(roll);

            var consoleOut = new StringWriter();
            Console.SetOut(consoleOut);

            this.TestedGame.WasCorrectlyAnswered();

            Approvals.VerifyAll(consoleOut.ToString(), roll + " " + nbPlayer);
        }
    }
}
