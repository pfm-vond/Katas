﻿#define CONTRACTS_FULL

namespace TDD_Idea
{
    using System;
    using System.Diagnostics.Contracts;

    // An IArray is an ordered collection of objects.    
    [ContractClass(typeof(AArrayContract))]
    public interface IArray
    {
        // The Item property provides methods to read and edit entries in the array.
        Object this[int index]
        {
            get;
            set;
        }

        int Count
        {
            get;

        }

        // Adds an item to the list.  
        // The return value is the position the new element was inserted in.
        int Add(Object value);

        // Removes all items from the list.
        void Clear();

        // Inserts value into the array at position index.
        // index must be non-negative and less than or equal to the 
        // number of elements in the array.  If index equals the number
        // of items in the array, then value is appended to the end.
        void Insert(int index, Object value);


        // Removes the item at position index.
        void RemoveAt(int index);
    }

    [ContractClassFor(typeof(IArray))]
    internal abstract class AArrayContract : IArray
    {
        int IArray.Add(Object value)
        {
            // Returns the index in which an item was inserted.
            Contract.Ensures(Contract.Result<int>() >= -1);
            Contract.Ensures(Contract.Result<int>() < ((IArray)this).Count);
            return default(int);
        }

        Object IArray.this[int index]
        {
            get
            {
                Contract.Requires(index >= 0);
                Contract.Requires(index < ((IArray)this).Count);
                return Contract.Result<object>();
            }
            set
            {
                Contract.Requires(index >= 0);
                Contract.Requires(index < ((IArray)this).Count);
            }
        }

        [Pure]
        int IArray.Count => Contract.Result<int>();

        void IArray.Clear()
        {
            Contract.Ensures(((IArray)this).Count == 0, "Array count can't be less than 0");
        }

        void IArray.Insert(int index, Object value)
        {
            Contract.Requires(index >= 0);
            Contract.Requires(index <= ((IArray)this).Count);  // For inserting immediately after the end.
            Contract.Ensures(((IArray)this).Count == Contract.OldValue(((IArray)this).Count) + 1);
        }

        void IArray.RemoveAt(int index)
        {
            Contract.Requires(index >= 0);
            Contract.Requires(index < ((IArray)this).Count);
            Contract.Ensures(((IArray)this).Count == Contract.OldValue(((IArray)this).Count) - 1);
        }
    }

    public class Array : IArray
    {
        public static void Main()
        {
            var ar = new Array();
            ar.Clear();
            Console.ReadLine();
        }

        private object[] innerArray = new object[0];

        public object this[int index]
        {
            get
            {
                return this.innerArray[index];
            }
            set
            {
                this.innerArray[index] = value;
            }
        }

        public int Count => -1;

        int IArray.Add(object value)
        {
            this.Insert(this.innerArray.Length, value);
            return this.Count;
        }

        public void Clear()
        {
            this.innerArray = new object[0];
        }

        public void Insert(int index, object value)
        {
            var arr = this.innerArray;
            this.innerArray = new object[arr.Length + 1];
            for (int i = 0; i < index; i++)
            {
                this.innerArray[i] = arr[i];
            }

            this.innerArray[index] = value;

            for (int i = index + 1; i <= arr.Length; i++)
            {
                this.innerArray[i] = arr[i - 1];
            }
        }

        public void RemoveAt(int index)
        {
            var arr = this.innerArray;
            this.innerArray = new object[arr.Length - 1];
            int j = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (j != index)
                {
                    this.innerArray[j++] = arr[i];
                }
            }
        }
    }
}
