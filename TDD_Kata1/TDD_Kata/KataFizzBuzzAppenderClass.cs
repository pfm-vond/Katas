﻿// <copyright file="KataFizzBuzzAppenderClass.cs" company="Talensoft">
//     Talensoft All rights reserved.
// </copyright>
// <author>VONDERSCHER Pierre</author>
namespace TDD_Kata
{
    using System;
    using System.Text;

    /// <summary>
    /// <see cref="KataFizzBuzzAppenderClass"/> leads the appending system link to the FizzBuzz Problem
    /// </summary>
    public class KataFizzBuzzAppenderClass
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="KataFizzBuzzAppenderClass"/> class
        /// </summary>
        /// <param name="shouldAppend">parameter indicating if the appending operation should be done</param>
        public KataFizzBuzzAppenderClass(bool shouldAppend)
        {
            this.ShouldAppend = shouldAppend;
            this.TobeAppend = new StringBuilder();
        }

        /// <summary>
        /// Gets or sets a value indicating whether the appending operation should be done
        /// </summary>
        private bool ShouldAppend { get; set; }

        /// <summary>
        /// Gets or sets The internal state of the current Appending process
        /// </summary>
        private StringBuilder TobeAppend { get; set; }

        /// <summary>
        /// append the parameter if the constructor allow it.
        /// </summary>
        /// <param name="s">parameter containing the string to be append.</param>
        /// <returns>this instances</returns>
        public KataFizzBuzzAppenderClass ThenAppend(string s)
        {
            if (this.ShouldAppend)
            {
                this.TobeAppend.Append(s);
            }

            return this;
        }

        /// <summary>
        /// allow to choose the destination of the append process
        /// </summary>
        /// <param name="sb">the destination</param>
        public void To(StringBuilder sb)
        {
            sb.Append(this.TobeAppend);
            this.TobeAppend.Clear();
        }
    }
}
