﻿// <copyright file="KataFizzBuzz.cs" company="Talensoft">
//     Talensoft All rights reserved.
// </copyright>
// <author>VONDERSCHER Pierre</author>
namespace TDD_Kata
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// main Class for FizzBuzz's Kata
    /// </summary>
    public class KataFizzBuzz
    {
        /// <summary>
        /// Gets or sets the list of number associated with a Special Rule.
        /// </summary>
        private Dictionary<int, string> SpecialRule { get; set; }

        /// <summary>
        /// Gets or sets the stringBuilder containing the final answer.
        /// </summary>
        private StringBuilder ResultHolder { get; set; }

        /// <summary>
        /// compute a string for the fizzBuzz problem based on the rules.
        /// </summary>
        /// <param name="n">the parameter number to mixed with the rules</param>
        /// <returns>a string generated based on the parameter and the registered special rules. </returns>
        public string ComputeFizzBuzz(int n)
        {
            this.InitializeAll();

            foreach (var divider in this.SpecialRule.Keys.OrderBy(i => i))
            {
                n.IfCanBeDividedPer(divider).ThenAppend(this.SpecialRule[divider]).To(this.ResultHolder);
            }

            return this.GetStringRepresentation(n);
        }

        /// <summary>
        /// Initialize the list of rules of the class in order to compute the result.
        /// </summary>
        private void InitializeList()
        {
            this.SpecialRule = new Dictionary<int, string> { { 3, "Fizz" }, { 5, "Buzz" } };
        }

        /// <summary>
        /// Initialize all the element of the class in order to compute the result.
        /// </summary>
        private void InitializeAll()
        {
            this.ResultHolder = new StringBuilder();
            this.InitializeList();
        }

        /// <summary>
        /// get the current result, after appending, for a number n
        /// </summary>
        /// <param name="n">the number to test</param>
        /// <returns>the current representation of FizzBuzz result</returns>
        private string GetStringRepresentation(int n)
        {
            if (this.ResultHolder.Length == 0)
            {
                return n.ToString();
            }
            else
            {
                return this.ResultHolder.ToString();
            }
        }
    }
}
