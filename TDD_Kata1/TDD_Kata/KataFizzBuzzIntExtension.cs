﻿// <copyright file="KataFizzBuzzIntExtension.cs" company="Talensoft">
//     Talensoft All rights reserved.
// </copyright>
// <author>VONDERSCHER Pierre</author>
namespace TDD_Kata
{
    /// <summary>
    /// Extension Class to integer for the FizzBuzz problem
    /// </summary>
    public static class KataFizzBuzzIntExtension
    {
        /// <summary>
        /// Test if the number can be divided by the divider and instantiate a instance of <see cref="KataFizzBuzzAppenderClass"/> with the result.
        /// </summary>
        /// <param name="n">the current integer</param>
        /// <param name="divider">the divider to test</param>
        /// <returns>an instance of <see cref="KataFizzBuzzAppenderClass"/></returns>
        public static KataFizzBuzzAppenderClass IfCanBeDividedPer(this int n, int divider)
        {
            return new KataFizzBuzzAppenderClass(n % divider == 0);
        }
    }
}