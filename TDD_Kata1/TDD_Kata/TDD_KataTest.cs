﻿// <copyright file="TDD_KataTest.cs" company="Talensoft">
//     Talensoft All rights reserved.
// </copyright>
// <author>VONDERSCHER Pierre</author>
namespace TDD_Kata
{
    using NUnit.Framework;

    /// <summary>
    /// Test class associated to KataFizzBuzz class
    /// </summary>
    [TestFixture]
    internal class TDD_KataTest
    {
        /// <summary>
        /// Gets or sets the instance of <see cref="KataFizzBuzz"/> currently tested
        /// </summary>
        private KataFizzBuzz Kata { get; set; }

        /// <summary>
        /// pre-initialization method called before each test method of this class run.
        /// </summary>
        [SetUp]
        public void PreTestInit()
        {
            this.Kata = new KataFizzBuzz();
        }

        /// <summary>
        /// test case of the functional request :
        /// Three Multiple should return Fizz
        /// </summary>
        /// <param name="n">the test value</param>
        [TestCase(3)]
        [TestCase(6)]
        [TestCase(51)]
        [TestCase(81)]
        public void ShouldReturnFizzWithThreeMultipleEntry(int n)
        {
            this.AssertFizzBuzz(n, "Fizz");
        }

        /// <summary>
        /// test case of the functional request :
        /// Five Multiple should return Buzz
        /// </summary>
        /// <param name="n">the test value</param>
        [TestCase(5)]
        [TestCase(10)]
        [TestCase(50)]
        [TestCase(80)]
        public void ShouldReturnBuzzWithFiveMultipleEntry(int n)
        {
            this.AssertFizzBuzz(n, "Buzz");
        }

        /// <summary>
        /// test case of the functional request :
        /// Fifteen Multiple should return FizzBuzz
        /// </summary>
        /// <param name="n">the test value</param>
        [TestCase(15)]
        [TestCase(30)]
        [TestCase(45)]
        [TestCase(60)]
        public void ShouldReturnFizzBuzzWithFifteenMultipleEntry(int n)
        {
            this.AssertFizzBuzz(n, "FizzBuzz");
        }

        /// <summary>
        /// test case of the functional request :
        /// default case should return itself
        /// </summary>
        /// <param name="n">the test value</param>
        [TestCase(1)]
        [TestCase(52)]
        [TestCase(37)]
        [TestCase(68)]
        public void ShouldReturntheNumberItselfMultipleEntry(int n)
        {
            this.AssertFizzBuzz(n, n.ToString());
        }

        /// <summary>
        /// Assert for a number that the method ComputeFizzBuzz return the value result
        /// </summary>
        /// <param name="n">the test case value</param>
        /// <param name="result">the test case expected result</param>
        private void AssertFizzBuzz(int n, string result)
        {
            string s = this.Kata.ComputeFizzBuzz(n);
            Assert.AreEqual(s, result);
        }
    }
}
