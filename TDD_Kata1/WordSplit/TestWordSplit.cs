﻿namespace WordSplit
{
    using NUnit.Framework;
    using System.Collections.Generic;
    using System.Linq;

    [TestFixture]
    public class TestWordSplit
    {
        ISplitter<string> WordSpliter;
        HashSet<string> AllExistingWords;

        [SetUp]
        public void SetUp()
        {
            // Arrange
            WordSpliter = new WordSpliter();
            AllExistingWords = new HashSet<string>();
            AllExistingWords.Add("fool");
            AllExistingWords.Add("feel");
            AllExistingWords.Add("a");
            AllExistingWords.Add("an");
            AllExistingWords.Add("another");
            AllExistingWords.Add("other");
            WordSpliter.Set(AllExistingWords);
        }

        [Test]
        public void SettingNeverThrowAnything()
        {
            // Arrange
            ISplitter<string> wordSpliter = new WordSpliter();
            HashSet<string> allExistingWords = new HashSet<string>();
            
            // Assert
            Assert.That(() => wordSpliter.Set(allExistingWords), Throws.Nothing);
        }

        [TestCase(null)]
        [TestCase("")]
        public void NullOrEmptyStringReturnEmptyIEnumarable(string toBeSplitted)
        {
            // Act
            IEnumerable<string> result = WordSpliter.Split(toBeSplitted);

            // Assert
            Assert.That(result, Is.Empty);
        }
        
        [TestCase("not An Existing Word")]
        public void UnparsableEntryReturnEmptyIEnumerable(string toBeSplitted)
        {
            // Act
            IEnumerable<string> result = WordSpliter.Split(toBeSplitted);

            // Assert
            Assert.That(result, Is.Empty);
        }

        [TestCase("free")]
        [TestCase("car")]
        public void UnambigousStringReturnitSelfInIEnumarable(string toBeSplitted)
        {
            // Arrange
            AllExistingWords.Add(toBeSplitted);

            // Act
            IEnumerable<string> result = WordSpliter.Split(toBeSplitted);

            // Assert
            var results = result.ToList();
            Assert.That(results.Count, Is.EqualTo(1));
            Assert.That(results.First(), Is.EqualTo(toBeSplitted));
        }

        [TestCase("free", "car")]
        [TestCase("imalsdkjhfhfldsflkksdjkdslfj", "stop")]
        public void UnambigousContenationReturnBothInIEnumarable(string firstWord, string secondWord)
        {
            // Arrange
            AllExistingWords.Add(firstWord);
            AllExistingWords.Add(secondWord);
            string toBeSplittedConcatenation = firstWord + secondWord;

            // Act
            IEnumerable<string> result = WordSpliter.Split(toBeSplittedConcatenation);

            // Assert
            var results = result.ToList();
            Assert.That(results.Count, Is.EqualTo(2));
            Assert.That(results[0], Is.EqualTo(firstWord));
            Assert.That(results[1], Is.EqualTo(secondWord));
        }
        
        [TestCase("animal", "stop")]
        public void AmbigousContenationReturnitSelfInIEnumarable(string firstWord, string secondWord)
        {
            // Arrange
            AllExistingWords.Add(firstWord);
            AllExistingWords.Add(secondWord);
            string toBeSplittedConcatenation = firstWord + secondWord;

            // Act
            IEnumerable<string> result = WordSpliter.Split(toBeSplittedConcatenation);

            // Assert
            var results = result.ToList();
            Assert.That(results.Count, Is.EqualTo(2));
            Assert.That(results[0], Is.EqualTo(firstWord));
            Assert.That(results[1], Is.EqualTo(secondWord));
        }
    }
}
