﻿using System.Collections.Generic;

namespace WordSplit
{
    internal interface ISplitter<T>
    {
        void Set(HashSet<T> allExistingWords);
        IEnumerable<T> Split(T toBeSplitted);
    }
}