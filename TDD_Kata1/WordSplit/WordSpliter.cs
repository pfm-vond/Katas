﻿namespace WordSplit
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    internal class WordSpliter : ISplitter<string>
    {
        private HashSet<string> AllExistingWords;

        public void Set(HashSet<string> allExistingWords)
        {
            AllExistingWords = allExistingWords;
        }

        public IEnumerable<string> Split(string toBeSplitted)
        {
            var list = new List<string>();
            Split(toBeSplitted, list);
            list.Reverse();

            return list;
        }

        public List<string> Split(string toBeSplitted, List<string> foundWords)
        {
            if (string.IsNullOrEmpty(toBeSplitted))
            {
                return foundWords;
            }

            IEnumerable<KeyValuePair<int, string>> possibleWords = FindFirstWord(toBeSplitted);
            foreach(var IndexWord in possibleWords)
            {
                var splittedEnd = Split(toBeSplitted.Substring(IndexWord.Key), foundWords);
                if (splittedEnd != null)
                {
                    splittedEnd.Add(IndexWord.Value);
                    return splittedEnd;
                }
            }

            return null;
        }

        private IEnumerable<KeyValuePair<int, string>> FindFirstWord(string toBeSplitted)
        {
            string currentWord = string.Empty;

            foreach (char c in toBeSplitted)
            {
                currentWord += c;
                if (AllExistingWords.Contains(currentWord))
                {
                    yield return new KeyValuePair<int, string>(currentWord.Length, currentWord);
                }
            }
        }
    }
}