﻿using System;
using System.Text;

namespace Trivia_csharp.Init
{
    public class GameRunner
    {
        private static bool notAWinner;

        public static void Main(String[] args)
        {
            var program = new StringBuilder();

            Game aGame = new Game();
            program.AppendLine("Game aGame = new Game();");
            program.AppendLine(string.Empty);

            aGame.isPlayable();
            program.AppendLine("aGame.isPlayable();");
            program.AppendLine(string.Empty);

            aGame.add("Chet");
            aGame.add("Pat");
            aGame.add("Sue");
            program.AppendLine("aGame.add(\"Chet\");");
            program.AppendLine("aGame.add(\"Pat\");");
            program.AppendLine("aGame.add(\"Sue\");");
            program.AppendLine(string.Empty);

            aGame.isPlayable();
            program.AppendLine("aGame.isPlayable();");
            program.AppendLine(string.Empty);

            Random rand = new Random();

            do
            {
                int rolled = rand.Next(5) + 1;
                aGame.roll(rolled);
                program.AppendLine($"aGame.roll({rolled});");

                bool wrongRight = rand.Next(10) > 7;
                if (wrongRight)
                {
                    notAWinner = aGame.wrongAnswer();
                    program.AppendLine("aGame.wrongAnswer();");
                    program.AppendLine(string.Empty);
                }
                else
                {
                    notAWinner = aGame.wasCorrectlyAnswered();
                    program.AppendLine("aGame.wasCorrectlyAnswered();");
                    program.AppendLine(string.Empty);
                }



            } while (notAWinner);

            Console.Out.WriteLine(program);

        }


    }

}
